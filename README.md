# Spring DDD kata

Kata to transform a "classic" Spring MVC project into a project with a domain heart, following DDD(domain driven design) principles. 

## How to use this project

Branches:
* mvc - classic Spring MVC implementation
* ddd - application with a domain heart

## Visuals
TBD

## Installation
TBD

## Usage
Use a support for a programming kata.


## Contributing
Closed for contribution for now

## Authors and acknowledgment
TBD

## License
 MIT license

## Project status
Just starting
